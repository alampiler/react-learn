import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import {Grid } from '@material-ui/core';
import TableBlock from './components/Table';
import Header from './components/Header';
import NavbarMenu from './components/Navbar';

import PaginationControlled from "./components/Pagination";
import Context from "./Context";

const theme = createMuiTheme({
	breakpoints: {
		values: {
			xs: 0,
			sm: 600,
			md: 960,
			lg: 1360,
			xl: 1920,
			xxl: 2480
		},
	},
	props: {
		MuiTypography: {
			variantMapping: {
				h1: 'h1',
				h2: 'h2',
				h3: 'h3',
				h4: 'h4',
				h5: 'h5',
				h6: 'h6',
				a: 'a',
				p: 'p',
        span: 'span',
				navItem: 'span',
        statusItem: 'span',
        viewsItem: 'span',
        tableTitle: 'h5'
			},
		},
	},
	typography: {
		fontFamily: 'Roboto',
		htmlFontSize: 10,
		h1: {
			fontSize: '1.8rem',
			fontWeight: 'normal',
		},
		h2: {
			fontSize: '1.6rem',
		},
		h3: {
			fontSize: '1.4rem',
		},
		navItem: {
			fontSize: '1.8rem',
			lineHeight: '2.1rem',
			fontWeight: 900
		}
	},
	palette: {
		primary: {
			dark: '#2D3748',
			main: '#2C5282',
			light: '#2A4365'
		},
		secondary: {
			main: '#A0AEC0'
		},
		error:{
			main: '#FC8181'
		},
    warning:{
		  main: '#FBD38D'
    },
    success:{
		  main: '#9AE6B4'
    }
	},
});
//
// const useStyles = makeStyles(({
// 	large: {
// 		height: '5rem',
// 		width: '5rem'
// 	},
// }));

// const Typography_p = withStyles({
// 	root: {
// 		color: "red"
// 	}
// })(Typography);

const infoList = [
  {id: 1, title: 'Design: A Survival Guide for Beginners1',subtitle: 'Posted 1 days ago', status: 'Published', views: '120', viewsStatus: 'up' },
  {id: 2, title: 'Design: A Survival Guide for Beginners2',subtitle: 'Posted 3 days ago', status: 'Draft', views: '0', viewsStatus: '' },
  {id: 3, title: 'Design: A Survival Guide for Beginners3',subtitle: 'Posted 3 days ago', status: 'Published', views: '80', viewsStatus: 'down' },
  {id: 4, title: 'Design: A Survival Guide for Beginners4',subtitle: 'Posted 5 days ago', status: 'Scheduled', views: '0', viewsStatus: '' },
  {id: 5, title: 'Design: A Survival Guide for Beginners5',subtitle: 'Posted 7 days ago', status: 'Published', views: '35', viewsStatus: 'up' },
  {id: 6, title: 'Design: A Survival Guide for Beginners6',subtitle: 'Posted 1 days ago', status: 'Published', views: '120', viewsStatus: 'up' },
  {id: 7, title: 'Design: A Survival Guide for Beginners7',subtitle: 'Posted 3 days ago', status: 'Draft', views: '0', viewsStatus: '' },
  {id: 8, title: 'Design: A Survival Guide for Beginners8',subtitle: 'Posted 3 days ago', status: 'Published', views: '80', viewsStatus: 'down' },
  {id: 9, title: 'Design: A Survival Guide for Beginners9',subtitle: 'Posted 5 days ago', status: 'Scheduled', views: '0', viewsStatus: '' },
  {id: 10, title: 'Design: A Survival Guide for Beginners10',subtitle: 'Posted 7 days ago', status: 'Published', views: '35', viewsStatus: 'up' },
  {id: 11, title: 'Design: A Survival Guide for Beginners11',subtitle: 'Posted 1 days ago', status: 'Published', views: '120', viewsStatus: 'up' },
  {id: 12, title: 'Design: A Survival Guide for Beginners12',subtitle: 'Posted 3 days ago', status: 'Draft', views: '0', viewsStatus: '' },
  {id: 13, title: 'Design: A Survival Guide for Beginners13',subtitle: 'Posted 3 days ago', status: 'Published', views: '80', viewsStatus: 'down' },
  {id: 14, title: 'Design: A Survival Guide for Beginners14',subtitle: 'Posted 5 days ago', status: 'Scheduled', views: '0', viewsStatus: '' },
  {id: 15, title: 'Design: A Survival Guide for Beginners15',subtitle: 'Posted 7 days ago', status: 'Published', views: '35', viewsStatus: 'up' },
  {id: 16, title: 'Design: A Survival Guide for Beginners16',subtitle: 'Posted 1 days ago', status: 'Published', views: '120', viewsStatus: 'up' },
  {id: 17, title: 'Design: A Survival Guide for Beginners17',subtitle: 'Posted 3 days ago', status: 'Draft', views: '0', viewsStatus: '' },
  {id: 18, title: 'Design: A Survival Guide for Beginners18',subtitle: 'Posted 3 days ago', status: 'Published', views: '80', viewsStatus: 'down' },
  {id: 19, title: 'Design: A Survival Guide for Beginners19',subtitle: 'Posted 5 days ago', status: 'Scheduled', views: '0', viewsStatus: '' },
  {id: 20, title: 'Design: A Survival Guide for Beginners20',subtitle: 'Posted 7 days ago', status: 'Published', views: '35', viewsStatus: 'up' },
];

function App() {
	// const classes = useStyles();
  const rowListLength = 5;
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(rowListLength);
  const paginationCount = Math.ceil( infoList.length / rowListLength);

  function setNextPageRow(page){
    setRowsPerPage(page * rowListLength);
    setPage((page - 1) * rowListLength);
  }

	return(
    <Context.Provider value={{setNextPageRow}}>
      <ThemeProvider theme={theme}>
        <Header />
        <Grid container direction="row">
          <Grid item xs={2}>
            <NavbarMenu />
          </Grid>
          <Grid item xs={10}>
            <TableBlock themeList={theme} curPage={page} pageList={rowsPerPage} content={infoList}/>
            <PaginationControlled curPage={page} pageList={rowsPerPage} count={paginationCount}/>
          </Grid>
        </Grid>
      </ThemeProvider>
    </Context.Provider>
	)
}

export default App;
