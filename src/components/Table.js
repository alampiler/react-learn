import React, {useContext} from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Button, Typography, SvgIcon  } from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

function DetectStatus(status){
  switch(status){
    case 'Published':
      return 'success-status';
    case 'Draft':
      return 'error-status';
    case 'Scheduled':
      return 'warning-status';
    default:
  }
}

function DetectViewsStatus(viewStatus){
  switch(viewStatus){
    case 'up':
      return (
        <SvgIcon>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="#9AE6B4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M16 12L12 8L8 12" stroke="#9AE6B4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M12 16V8" stroke="#9AE6B4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
        </SvgIcon>
      );
    case '':
      return '';
    case 'down':
      return (
        <SvgIcon>
          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11 21C16.5228 21 21 16.5228 21 11C21 5.47715 16.5228 1 11 1C5.47715 1 1 5.47715 1 11C1 16.5228 5.47715 21 11 21Z" stroke="#FC8181" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M7 11L11 15L15 11" stroke="#FC8181" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M11 7L11 15" stroke="#FC8181" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
        </SvgIcon>
      );

    default:
  }
}

function TableBlock(params){
  const [newList] = React.useState(params.content);
  console.log('Curpage: ', params.curPage);
  console.log('pageList: ', params.pageList);
  return(
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography variant="tableTitle">Title</Typography>
            </TableCell>
            <TableCell>
              <Typography variant="tableTitle">Status</Typography>
            </TableCell>
            <TableCell>
              <Typography variant="tableTitle">Stats</Typography>
            </TableCell>
            <TableCell>
              <Button variant="contained" color="primary">
                <SvgIcon>
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10 15.8333L15.8333 10L18.3333 12.5L12.5 18.3333L10 15.8333Z" stroke="#FFFCFE" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M15 10.8333L13.75 4.58329L1.66663 1.66663L4.58329 13.75L10.8333 15L15 10.8333Z" stroke="#FFFCFE" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M1.66663 1.66663L7.98829 7.98829" stroke="#FFFCFE" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M9.16667 10.8333C10.0871 10.8333 10.8333 10.0871 10.8333 9.16667C10.8333 8.24619 10.0871 7.5 9.16667 7.5C8.24619 7.5 7.5 8.24619 7.5 9.16667C7.5 10.0871 8.24619 10.8333 9.16667 10.8333Z" stroke="#FFFCFE" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </SvgIcon>
                Add new
              </Button>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {
              newList.slice(params.curPage ,params.curPage * params.pageList + params.pageList).map(function(elem, index, row){
                return (
                  <TableRow key={elem.id}>
                    <TableCell>
                      <Typography variant="h3">{elem.title}</Typography>
                      <Typography variant="span">{elem.subtitle}</Typography>
                    </TableCell>
                    <TableCell className={DetectStatus(elem.status)}>
                      <Typography variant="statusItem">{elem.status}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="viewsItem">{elem.views}</Typography>
                      <Typography variant="span">views</Typography>
                      {DetectViewsStatus(elem.viewsStatus)}
                    </TableCell>
                    <TableCell>
                      <MoreHorizIcon />
                    </TableCell>
                  </TableRow>
                )
              })
            }
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default TableBlock;