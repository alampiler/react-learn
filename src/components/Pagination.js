import React, {useContext, useEffect} from 'react';
import { Grid } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import Context from '../Context'

function PaginationControlled(params){
  const [curPage, setPage] = React.useState(params.curPage);

  const {setNextPageRow} = useContext(Context);
  const handleChange = function(event, value){
    setPage(value);
    setNextPageRow(+event.target.textContent);
  };

  return (
      <Grid container direction="row" justify="center">
        <Pagination count={params.count} hideNextButton={true} hidePrevButton={true}  page={curPage} onChange={handleChange} />
      </Grid>
  );
}

export default PaginationControlled;