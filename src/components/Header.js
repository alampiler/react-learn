import React from 'react';
import { Button, Grid, Avatar, AppBar } from '@material-ui/core';


function Header(){
	return(
		<AppBar position="static">
			<Grid container >
				<Grid xs={6} container item direction="row" justify="flex-start" alignItems="center">
					<img src={require("../images/logo.svg")} alt="logo"/>
				</Grid>
				<Grid xs={6} container item direction="row" justify="flex-end" alignItems="center">
					<Button size="medium">Pro plan</Button>
					<Avatar src={require("../images/avatar.png")} alt="avatar"/>
				</Grid>
			</Grid>
		</AppBar>
	)
}

export default Header;