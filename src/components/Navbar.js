import React from 'react';
import { Typography, Box, MenuList, MenuItem } from '@material-ui/core';

const manageList = [
  {id: 1, title: 'View site', img:'../images/home-icon.svg'},
  {id: 2, title: 'Create page', img:'../images/create-icon.svg'},
  {id: 3, title: 'Blog articles', img:'../images/blog-icon.svg'},
  {id: 4, title: 'Users', img:'../images/files-icon.svg'},
  {id: 5, title: 'Subscription', img:'../images/subscription.svg'},
  {id: 6, title: 'Archive pages', img:'../images/archive-icon.svg'}
];

const featuresList = [
  {id: 1, title: 'Themes', img:'../images/themes-icon.svg'},
  {id: 2, title: 'Plugin', img:'../images/plugins-icon.svg'},
  {id: 3, title: 'Upgrade plans', img:'../images/upgrade-icon.svg'},
];

function NavbarMenu(){
	return (
    <MenuList>
      <Box>
        <Typography variant="h6">Manage</Typography>
      </Box>
      {
        manageList.map(function(elem, index){
          return (
            <MenuItem key={elem.id}>
              {/*<img src={elem.img} alt="logo"/>*/}
              <Typography variant="navItem">{elem.title}</Typography>
            </MenuItem>
          )
        })
      }
      <Box>
        <Typography variant="h6">Pro features</Typography>
      </Box>
      {
        featuresList.map(function(elem, index){
          return (
            <MenuItem key={elem.id}>
              {/*<img src={require("../images/" + {elem.img} + '"')} alt="icon"/>*/}
              <Typography variant="navItem">{elem.title}</Typography>
            </MenuItem>
          )
        })
      }
    </MenuList>
  )
}

export default NavbarMenu;